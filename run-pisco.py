#!/usr/bin/env python3
# -*- coding: utf-8 -*-


"""Convenience wrapper for running pisco directly from source tree."""


from pisco.pisco import main


if __name__ == '__main__':
    main()

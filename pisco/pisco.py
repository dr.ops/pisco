#!/usr/bin/env python3
# -*- coding: utf-8 -*-


"""pisco.pisco: provides entry point main()."""


__version__ = "0.2.0"


import os
import sys
import configparser
from pisco.sync.status import SyncStatus
from pisco.sync.object import SyncObject
from pisco.converter.orgmode import Orgmode
from pisco.converter.vdir import VDirectory

cachePath = ""
syncUsed = ""


def main():
    print("Executing pisco version %s." % __version__)

    cfg = getconfig("pisco")

    if "DEFAULT" in cfg:
        default = cfg["DEFAULT"]
        if "sync" in default:
            defaultsync = default["sync"]

            if defaultsync == "":
                sys.exit("Default sync is empty in configuration")

            if "sync " + defaultsync not in cfg:
                sys.exit("Default sync '" + defaultsync + "' not found")

            cfgsync = cfg["sync " + defaultsync]

            global syncUsed
            syncUsed = defaultsync

            if "target_a" not in cfgsync:
                sys.exit("No File_A in sync '" + defaultsync + "'")

            if "target_b" not in cfgsync:
                sys.exit("No File_B in sync '" + defaultsync + "'")

            targetA = cfgsync["target_a"]
            targetB = cfgsync["target_b"]

            if "target " + targetA not in cfg:
                sys.exit("target_a not found '" + targetA + "'")

            if "target " + targetB not in cfg:
                sys.exit("target_b not found '" + targetB + "'")

            cfgA = cfg["target " + targetA]
            cfgB = cfg["target " + targetB]

            typeA = cfgA["type"]
            locationA = cfgA["location"]
            typeB = cfgB["type"]
            locationB = cfgB["location"]

    print("Syncing " + typeA + ":" + locationA +
          " with " + typeB + ":" + locationB)

    doSync(typeA, locationA, typeB, locationB)


def doSync(typeA, locationA, typeB, locationB):
    status = SyncStatus(cachePath, syncUsed)

    if typeA == "orgmode":
        targetA = Orgmode(locationA)
    elif typeA == "vdir":
        targetA = VDirectory(locationA)

    if typeB == "orgmode":
        targetB = Orgmode(locationB)
    elif typeB == "vdir":
        targetB = VDirectory(locationB)

    a = targetA.read()
    b = targetB.read()

    for key in status.appeared(a):
        b[key] = a[key]
        b[key].status = "+"
        a[key].status = "N"
        status.add(key, a[key].chkSum)
        print("Added " + key + " on " + locationB)

    for key in status.appeared(b):
        a[key] = b[key]
        a[key].status = "+"
        b[key].status = "N"
        status.add(key, b[key].chkSum)
        print("Added " + key + " on " + locationA)

    for key in status.disappeared(a):
        b[key].status = "-"
        status.delete(key)
        print("Deleted " + key + " on " + locationB)

    for key in status.disappeared(b):
        a[key].status = "-"
        status.delete(key)
        print("Deleted " + key + " on " + locationA)

    for key in status.orphans(a, b):
        status.delete(key)
        print("Orphanized status" + key)

    targetA.write(a)
    targetB.write(b)

    status.write()


def getconfig(programname):

    if 'APPDATA' in os.environ:
        confighome = os.environ['APPDATA']
    elif 'XDG_CONFIG_HOME' in os.environ:
        confighome = os.environ['XDG_CONFIG_HOME']
    else:
        confighome = os.path.join(os.environ['HOME'], '.config')
    configpath = os.path.join(confighome, programname)
    global cachePath
    cachePath = os.path.join(confighome, programname, "status")

    if not os.path.exists(configpath):
        os.makedirs(configpath)

    if not os.path.exists(cachePath):
        os.makedirs(cachePath)

    configfile = configpath + "/config"
    config = configparser.ConfigParser()

    if not os.path.isfile(configfile):
        config['DEFAULT'] = {}
        config['target orgcalendar'] = {}
        config['target orgcalendar']['type'] = 'orgmode'
        config['target orgcalendar']['location'] = '/home/dr/calendar.org'

        config['target vdircalendar'] = {}
        config['target vdircalendar']['type'] = 'vdir'
        config['target vdircalendar']['location'] = '/home/dr/vdir'

        config['sync calendar'] = {}
        config['sync calendar']['File_A'] = 'orgcalendar'
        config['sync calendar']['File_B'] = 'vdircalendar'

        config['DEFAULT']['conflictresolution'] = 'duplicate'
        config['DEFAULT']['sync'] = 'calendar'

        with open(configfile, 'w') as cfgfile:
            config.write(cfgfile)

    config.read(configfile)
    return config

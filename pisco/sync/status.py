import os
import pickle

class SyncStatus:

    def __init__(self, cachePath, syncUsed):
        self.file = cachePath + "/" + syncUsed
        if not os.path.isfile(self.file):
            self.status = {}
        else:
            with open(self.file, 'rb') as f:
                self.status = pickle.load(f)

    def write(self):
        with open(self.file, 'wb') as f:
            pickle.dump(self.status, f)

    def disappeared(self, dictionary):

        statusSet = set(self.status.keys())
        dicSet = set(dictionary.keys())

        return statusSet - dicSet

    def orphans(self, a, b):
        statusSet = set(self.status.keys())
        aSet = set(a.keys())
        bSet = set(b.keys())
        tempSet = statusSet - aSet
        return tempSet - bSet

    def appeared(self, dictionary):

        statusSet = set(self.status.keys())
        dicSet = set(dictionary.keys())

        return dicSet - statusSet

    def add(self, key, chkSum):
        self.status[key] = chkSum, chkSum

    def delete(self, key):
        del self.status[key]
        print(key in self.status)

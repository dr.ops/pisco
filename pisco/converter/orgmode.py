import os
from ..sync.object import SyncObject
from .pisco import PiscoObject


class Orgmode:
    def __init__(self, location):
        self.location = location
        self.syncObjects = {}
        self.piscoObject = None

    def makeSyncObjects(self,line):
        if line.startswith("*"):
            if self.piscoObject is not None:
                syncObj = SyncObject(self.piscoObject)
                self.syncObjects[syncObj.id] = syncObj
            self.piscoObject = PiscoObject("VTODO","guid")
        else:
            if line.startswith("UID:"):
                first, *middle, last = line.split()
                self.piscoObject.uid =  last
 
    
    def read(self):
        if not os.path.isfile(self.location):
            self.syncObjects = {}
        else:
            with open(self.location) as f:
                for line in f:
                   self.makeSyncObjects(line)
                syncObj = SyncObject(self.piscoObject)
                self.syncObjects[syncObj.id] = syncObj
            
        return self.syncObjects

    def write(self, dict):
        try:
            os.remove(self.location)
        except OSError:
            pass

        orgFile = open(self.location, 'w')
        for syncObject in dict.values():
            if syncObject.status != "-":
                piscoObject = syncObject.payLoad
                if piscoObject.type == "VTODO":
                    sequence = None
# .strftime("<%Y-%m-%d %a %H:%M>")
                    orgFile.write("* " + piscoObject.status + " " + piscoObject.summary + "\n")
                    if piscoObject.scheduled is not None:
                        orgFile.write("SCHEDULED: " + piscoObject.scheduled + "\n")
                    orgFile.write(":SYNC:\n")
                    orgFile.write("UID: " + piscoObject.uid + "\n")

                    orgFile.write(":END:\n")
                elif piscoObject.type == "VEVENT":    
                    sequence = None
# .strftime("<%Y-%m-%d %a %H:%M>")
                    if piscoObject.scheduled is not None:
                        print(piscoObject.scheduled + piscoObject.summary + "\n")
                        orgFile.write(piscoObject.scheduled + piscoObject.summary + "\n")
                    orgFile.write(":SYNC:\nUID: "+ piscoObject.uid + "\n:END:\n")

        orgFile.close()



import vobject
from os import walk
from ..sync.object import SyncObject
from .pisco import PiscoObject

class VDirectory:
    def __init__(self, location):
        self.location = location
        self.syncObjects = {}

    def read(self):
        f = []
        for (dirpath, dirnames, filenames) in walk(self.location):
            f.extend(filenames)
            break

        for filename in filenames:
            f = open(dirpath + "/" + filename, 'r', encoding='utf-8')
            text = f.read()
            f.close()

            obj = vobject.readOne(text)

            for cmpt in obj.getChildren():
                if cmpt.name in ["VTODO", "VEVENT"]:
                    lines = cmpt.getChildren()
                    base = None
                    for l in lines:
                        if l.name == "UID":
                            base = PiscoObject(cmpt.name, l.value)
                    lines = cmpt.getChildren()
                    for l in lines:
                        if l.name == "UID":
                            pass
                        elif l.name == "SCHEDULED":
                            base.scheduled = cmpt.scheduled.value
                        elif l.name == "DTSTAMP":
                            base.dtstamp = cmpt.dtstamp.value
                        elif l.name == "SUMMARY":
                            base.summary = cmpt.summary.value
                        elif l.name == "COMPLETED":
                            base.completed = cmpt.completed.value
                        elif l.name == "LAST-MODIFIED":
                            base.last_modified = cmpt.last_modified.value
                        elif l.name == "STATUS":
                            base.status = {"NEEDS-ACTION": "TODO",
                                          "COMPLETED": "DONE",
                                          "IN-PROCESS": "WORKING",
                                          "CANCELLED": "CANCELLED"}. \
                                          get(cmpt.status.value,
                                              cmpt.status.value)
                        elif l.name == "SEQUENCE":
                            base.sequence = cmpt.sequence.value
                        elif l.name == "DTSTART":
                            base.scheduled = cmpt.dtstart.value
                        elif l.name == "PERCENT-COMPLETE":
                            base.percent_complete = cmpt.percent_complete.value
                        elif l.name == "LOCATION":
                            base.location = cmpt.location.value
                        elif l.name == "CREATED":
                            base.created = cmpt.created.value
                        elif l.name == "PRIORITY":
                            base.priority = cmpt.priority.value
                        elif l.name == "X-OC-HIDESUBTASKS":
                            base.x_oc_hidesubtasks = cmpt.x_oc_hidesubtasks.value
                        else:
                            print(l.name)

            syncObj = SyncObject(base)
            self.syncObjects[syncObj.id] = syncObj

        return self.syncObjects

    def write(self, dict):
        pass

import hashlib


class PiscoObject:

    def __init__(self, type, uid):
        self.type = type
        self.uid = uid
        self.__status = ""
        self.__summary = ""
        self.__scheduled = None

    @property
    def scheduled(self):
        if self.__scheduled is None:
            return None
        else:
            return self.__scheduled.strftime("<%Y-%m-%d %a %H:%M>")

    @scheduled.setter
    def scheduled(self, value):
        self.__scheduled = value

    @property
    def status(self):
        return self.__status

    @status.setter
    def status(self, value):
        self.__status = value

    @property
    def summary(self):
        return self.__summary

    @summary.setter
    def summary(self, value):
        self.__summary = value

    def chkSum(self):
        # self.chkSum = hashlib.sha256(vObject.serialize()
        #                             .encode("utf-8")).hexdigest()
        # print(vObject.serialize() .encode("utf-8"))
        return("chksum")
